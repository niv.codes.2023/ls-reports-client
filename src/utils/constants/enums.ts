// supported dataTypes
export enum dataTypes {
    number = "number",
    string = "string",
    date = "date",
    boolean = "boolean",
    object = "object"
}

export enum objectInputTypes {
    select = 'select',
    propertyInput = 'propertyInput'
}

export enum dateInputTypes {
    select = 'select',
    firstDateInput = 'firstDateInput',
    secondDateInput = "secondDateInput"
}

export enum numberInputTypes {
    select = 'select',
    firstNumberInput = 'firstNumberInput',
    secondNumberInput = "secondNumberInput"
}

export enum numberFilterModes {
    All = "All",
    Equal = "Equal",
    LessThan = "Less than",
    LessThanOrEqual = "Less than or Equal",
    GreaterThan = "Greater than",
    GreaterThanOrEqual = "Greater than or Equal",
    Between = "Between"
}

export enum dateFilterModes {
    All = "All",
    Equal = "Equal",
    Before = "Before",
    BeforeAndEqual = "Before And Equal",
    After = "After",
    AfterAndEqual = "After And Equal",
    Between = "Between"
}