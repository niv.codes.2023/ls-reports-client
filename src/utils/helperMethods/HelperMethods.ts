import * as moment from "moment";

export class HelperMethods {
    static isValidDate(date: any) {
        return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
    }

    // init date object to be able to compare it to date object from basic html input
    static DateObjectToCompare(date: Date): Date {
        const mDate = moment(date);
        // the html object we use now does not have minutes or hours or seconds
        if (mDate.isValid()) {
            mDate.set('hours', 0);
            mDate.set('minutes', 0);
            mDate.set('seconds', 0);
            mDate.set('milliseconds', 0);
        } else {
            console.error('Invalid Date Object!')
        }
        return mDate.toDate();
    }

    // get an object, and string, for example { a:{ b: 5 }}, 'a.b', returns 5
    static readProp = (obj, propsString) => {
        const accessorsArr = propsString.split('.');
        let finalVal = obj;
        let prop;
        for (let index = 0; index < accessorsArr.length; index++) {
            if (finalVal) {
                prop = accessorsArr[index];
                finalVal = finalVal[prop];
            }
        }
        return finalVal;
    }
}