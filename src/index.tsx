import * as React from 'react';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import ReactTable from 'react-table';
import { LSColumn } from './modules/LSColumn';
import { XLSXExten, DEFAULT_SHEET_NAME, MOMENT_FORMAT } from './utils/constants/constants';
import { HelperMethods } from './utils/helperMethods/HelperMethods';
import Server from './utils/server/server';
import { GET_REPORT } from './utils/server/urls';

export interface IProps {
	baseURL: string;
	reportName: string;
}
 
export interface IState {
	columns: LSColumn[];
	data: object[];
	schema: object[];
}

export default class LSTable extends React.Component<IProps, IState> {
	state = {
		columns: [],
		data: [],
		schema: []
	}

	private table = React.createRef<ReactTable<object>>();

	componentDidMount() {
		const { baseURL, reportName } = this.props;
		Server.initBaseUrl(baseURL)

		const server = new Server();
		server.get(GET_REPORT(reportName))
					.then(res => {
						const { data = {} } = res.data;
						const serverData = data.data;
						const { schema } = data;

						const columns = LSColumn.parseData(schema);
						this.setState({ data: serverData, columns, schema })
					})
	}

	private getTableData = () => {
		const tableRef: any = this.table.current;
		if (tableRef) {
			return tableRef.getResolvedState().sortedData.map(r => r._original);
		}
		return [];
	}

	// prepares the data to the XLSX module format
	private prepareDataForExcel = () => {
		const { schema } = this.state;
		const schemaCopy = schema.slice();
		const headers = schemaCopy.map((h: any) => h.Header);

		const data = this.getTableData();
		let dataArr = data as Array<object>;
		dataArr = dataArr.map(obj => Object.keys(obj).map(key => this.prettifyData(obj[key])));
		dataArr.unshift(headers);

		return dataArr;
	}

	// convert specific data types to pretty string to show in the Excel
	private prettifyData = (val) => {
		if (typeof val === "object" && !HelperMethods.isValidDate(val)) {
			return JSON.stringify(val, null, 2);
		}
		return val;
	}

	/**
	 * exportToExcel
	 *
	 */
	public exportToExcel = () => {
		const { reportName } = this.props;

		const mNow = moment(new Date());
		const dateInFileName = mNow.format(MOMENT_FORMAT);
		const fileName = `${reportName} ${dateInFileName}${XLSXExten}`

		const data = this.prepareDataForExcel();
		const workBook = XLSX.utils.book_new();
		const workSheet = XLSX.utils.aoa_to_sheet(data as any[]);
		
        XLSX.utils.book_append_sheet(workBook, workSheet, DEFAULT_SHEET_NAME);
		XLSX.writeFile(workBook, fileName);
	}

	render() {
		const { columns, data } = this.state;
  
		return (
			<ReactTable
				ref={this.table}
				data={data}
				columns={columns}
			/>
		);
	}
}